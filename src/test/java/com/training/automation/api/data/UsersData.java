package com.training.automation.api.data;


import io.restassured.response.Response;

public class UsersData {
    private static String email;
    private static String name;
    private static String phoneNumber;
    private static int id;

    private static Response getSingleUserResponse;
    private static Response createUserResponse;
    private static Response deleteUserResponse;

    public static void setDeleteUserResponse(Response deleteUserResponse) {
        UsersData.deleteUserResponse = deleteUserResponse;
    }

    public static Response getDeleteUserResponse() {
        return deleteUserResponse;
    }

    public static int getId() {
        return id;
    }

    public static void setId(int id) {
        UsersData.id = id;
    }

    public static Response getGetSingleUserResponse() {
        return getSingleUserResponse;
    }

    public static void setGetSingleUserResponse(Response getSingleUserResponse) {
        UsersData.getSingleUserResponse = getSingleUserResponse;
    }

    public static Response getCreateUserResponse() {
        return createUserResponse;
    }

    public static void setCreateUserResponse(Response createUserResponse) {
        UsersData.createUserResponse = createUserResponse;
    }

    public static String getEmail() {
        return email;
    }

    public static void setEmail(String email) {
        UsersData.email = email;
    }

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        UsersData.name = name;
    }

    public static String getPhoneNumber() {
        return phoneNumber;
    }

    public static void setPhoneNumber(String phoneNumber) {
        UsersData.phoneNumber = phoneNumber;
    }
}
