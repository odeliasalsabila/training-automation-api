package com.training.automation.api.steps;

import com.training.automation.api.api.UsersController;
import com.training.automation.api.data.UsersData;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.steps.ScenarioSteps;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class UsersSteps extends ScenarioSteps {

    @Given("^\\[api\\] set email \"([^\"]*)\"$")
    public void apiSetEmail(String emailTxt){
        UsersData.setEmail(emailTxt);
    }

    @And("^\\[api\\] set name \"([^\"]*)\"$")
    public void apiSetName(String nameTxt) {
        UsersData.setName(nameTxt);
    }

    @And("^\\[api\\] set number \"([^\"]*)\"$")
    public void apiSetNumber(String numberTxt) {
        UsersData.setPhoneNumber(numberTxt);
    }

    @When("^\\[api\\] send create user request$")
    public void apiSendCreateUserRequest() {
        UsersData.setCreateUserResponse(UsersController.createUser());
    }

    @Then("^\\[api\\] status must be '(\\d+)'$")
    public void apiStatusMustBe(int statusTxt) {
        assertThat(UsersData.getCreateUserResponse().statusCode(), equalTo(statusTxt));

        // Assert statusCode directly from the Response method
        UsersData.getCreateUserResponse().
                then().
                statusCode(statusTxt);
    }

    @Given("^\\[api\\] set id '(\\d+)'$")
    public void apiSetId(int id) {
        UsersData.setId(id);
    }

    @When("^\\[api\\] send delete request$")
    public void apiSendDeleteRequest() {
        UsersData.setDeleteUserResponse(UsersController.deleteUser());
    }

    @Then("^\\[api\\] status delete must be '(\\d+)'$")
    public void apiStatusDeleteMustBe(int statusTxt) {
        assertThat(UsersData.getDeleteUserResponse().statusCode(), equalTo(statusTxt));

        // Assert statusCode directly from the Response method
        UsersData.getDeleteUserResponse().
                then().
                statusCode(statusTxt);
    }

}
