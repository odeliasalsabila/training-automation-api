package com.training.automation.api.api;

public class DeleteUserRequest {
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
