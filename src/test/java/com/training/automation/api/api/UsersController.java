package com.training.automation.api.api;

import com.training.automation.api.data.UsersData;
import io.restassured.response.Response;
import static net.serenitybdd.rest.SerenityRest.given;

public class UsersController {
    public static Response createUser() {
        CreateUserRequest request = new CreateUserRequest();
        request.setEmail(UsersData.getEmail());
        request.setName(UsersData.getName());
        request.setPhoneNumber(UsersData.getPhoneNumber());

        return  given().
                header("Content-Type", "application/json").
                header("Accept", "application/json").
                body(request).
                when().
                post("http://localhost:17001/api/member");
    }

    public static Response deleteUser() {
        DeleteUserRequest request = new DeleteUserRequest();
        request.setId(UsersData.getId());

        return  given().
                header("Content-Type", "application/json").
                header("Accept", "application/json").
                body(request).
                when().
                delete("http://localhost:17001/api/member?id=" + UsersData.getId());
    }

}
