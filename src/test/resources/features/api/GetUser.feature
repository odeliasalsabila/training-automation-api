@API
Feature: get user in member management

  Scenario: deleting user by her/his id success
    Given [api] set id '2'
    When [api] send delete request
    Then [api] status delete must be '200'

  Scenario: deleting user by her/his id failed
    Given [api] set id '1'
    When [api] send delete request
    Then [api] status delete must be '400'