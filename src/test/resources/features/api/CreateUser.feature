Feature: create user member management

  Scenario: creating user member success
    Given [api] set email "belaaa@gmail.com"
    And [api] set name "salsabilaa"
    And [api] set number "089087451166"
    When [api] send create user request
    Then [api] status must be '200'

   Scenario: creating use member failed
     Given [api] set email "salsabilaa@gmail.com"
     And [api] set name "odelia"
     And [api] set number "089508775103"
     When [api] send create user request
     Then [api] status must be '400'
